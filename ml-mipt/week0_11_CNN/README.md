week 11 practice on image classification:
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/ml-mipt/ml-mipt/blob/basic_s20/week0_11_CNN/week0_11_cnn_seminar.ipynb)


__Further readings__:
* [en] Convolutional Neural Networks: Architectures, Convolution / Pooling Layers: http://cs231n.github.io/convolutional-networks/
* [en] Understanding and Visualizing Convolutional Neural Networks: http://cs231n.github.io/understanding-cnn/
* [en] CS231n notes on data preparation: http://cs231n.github.io/neural-networks-2/